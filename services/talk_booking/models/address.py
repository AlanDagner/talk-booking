#class Address:
#    def __init__(self, street, city, state, country):
#        self.street = street
#        self.city = city
#        self.state = state
#        self.country = country

from pydantic import BaseModel

class Address(BaseModel):
    street: str
    city: str
    state: str
    country: str