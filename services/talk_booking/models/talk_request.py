import datetime
from enum import Enum

from pydantic import BaseModel, EmailStr, PositiveInt

from .address import Address

class TalkRequestStatus(str, Enum):
    PENDING = "PENDING"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"

class TalkRequest(BaseModel):
    id: str
    event_time: datetime.datetime
    address: Address
    topic: str
    duration_in_minutes: PositiveInt
    requester: EmailStr
    status: TalkRequestStatus

    suggestion:-0+0
    @property
    def is_rejected(self):
        return self.status == TalkRequestStatus.REJECTED

    suggestion:-0+1
    @property
    def is_not_rejected(self):
        return self.status == TalkRequestStatus.REJECTED


    def test_talk_request_is_rejected():
        event_time = datetime.datetime.utcnow()
        talk_request = TalkRequest(
            id="request_id",
            event_time=event_time,
            address=Address(
                street="Sunny street 42",
                city="Awesome city",
                state="Best state",
                country="Ireland",
            ),
            duration_in_minutes=45,
            topic="Python type checking",
            requester="john@doe.com",
            status="PENDING",
        )

        talk_request.is_rejected